<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky">Title:</th>
    <th class="tg-0pky">Set up convenient services for customers</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">Value Statement:</td>
    <td class="tg-0pky">As a hotel customer,I want to use the hotel's convenient services for entertainment and relaxation</td>
  </tr>
  <tr>
    <td class="tg-0pky" rowspan="12">Acceptance Criteria:</td>
    <td class="tg-0pky">-The receptionist must log in to the system and the customer must make a reservation</td>
  </tr>
  <tr>
    <td class="tg-0pky">-A use case is performed when a customer wants to order a service by coming directly to the counter or calling</td>
  </tr>
  <tr>
    <td class="tg-0pky">1. The reception department chooses the function to add services to customers.</td>
  </tr>
  <tr>
    <td class="tg-0pky">2. The system displays the booked room information form. Information includes:</td>
  </tr>
  <tr>
    <td class="tg-0pky">+ Room code</td>
  </tr>
  <tr>
    <td class="tg-0pky">+ Customer name</td>
  </tr>
  <tr>
    <td class="tg-0pky">+ Service name</td>
  </tr>
  <tr>
    <td class="tg-0pky">3. The reception department searches for room information where the customer requests to book the service.</td>
  </tr>
  <tr>
    <td class="tg-0pky">4. Receptionist conducts additional services.</td>
  </tr>
  <tr>
    <td class="tg-0pky">5. The receptionist selects the "Add SERVICE" button to complete adding services for the customer.</td>
  </tr>
  <tr>
    <td class="tg-0pky">6. The system checks the data the receptionist has just manipulated.</td>
  </tr>
  <tr>
    <td class="tg-0pky">7. The system saves customer service information.</td>
  </tr>
</tbody>
</table>